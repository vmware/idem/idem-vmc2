==========================
Contributing to idem-vmc
==========================

The idem-vmc project team welcomes contributions from the community. Before
you start working with idem-vmc, please read our [Developer Certificate of
Origin](https://cla.vmware.com/dco). All contributions to this repository must
be signed as described on that page. Your signature certifies that you wrote
the patch or have the right to pass it on as an open-source patch.

Ways to Contribute
------------------

We welcome all contributions, not just contributions to the code. In addition
to contributing to the code, you can help the project by:

* Writing, reviewing, and revising documentation, modules, and tutorials.
* Opening issues on bugs, feature requests, or docs.
* Spreading the word about how this project.

Code Contribution Flow
----------------------

This is a rough outline of what a contributor's workflow looks like:

#. Create a topic branch from where you want to base your work
#. Make commits of logical units
#. Make sure your commit messages are in the proper format (see below)
#. Push your changes to a topic branch in your fork of the repository
#. Submit a merge / pull request

Example:

.. code-block:: bash

    git remote add upstream https://github.com/vmware/idem-vmc.git
    git checkout -b my-new-feature master
    git commit -a
    git push origin my-new-feature

Staying In Sync With Upstream
+++++++++++++++++++++++++++++

When your branch gets out of sync with the vmware/master branch, use the following to update:

.. code-block:: bash

    git checkout my-new-feature
    git fetch -a
    git pull --rebase upstream master
    git push --force-with-lease origin my-new-feature

Updating Pull / Merge Requests
++++++++++++++++++++++++++++++

If your merge request fails to pass CI or needs changes based on code review,
you'll most likely want to squash these changes into existing commits.

If your pull request contains a single commit or your changes are related to
the most recent commit, you can simply amend the commit.

.. code-block:: bash

    git add .
    git commit --amend
    git push --force-with-lease origin my-new-feature

If you need to squash changes into an earlier commit, you can use:

.. code-block:: bash

    git add .
    git commit --fixup 
    git rebase -i --autosquash master
    git push --force-with-lease origin my-new-feature

Be sure to add a comment to the merge request indicating your new changes are
ready to review, as GitHub does not generate a notification when you git push.

Formatting Commit Messages
++++++++++++++++++++++++++

We follow the conventions on `How to Write a Git Commit Message
`__.

Be sure to include any related GitHub issue references in the commit message.
See `GFM syntax
`__
for referencing issues and commits.

Reporting Bugs and Creating Issues
----------------------------------

When opening a new issue, try to roughly follow the commit message format
conventions above.

TL;DR Quickstart Example
========================

#. Have pre-requisites completed:

   * ``git``
   * ``nox``
   * ``pre-commit``
   * Python 3.6+

#. Fork the project
#. ``git clone`` your fork locally
#. Create your feature branch (ex. ``git checkout -b amazing-feature``)
#. Setup your local development environment

   .. code-block:: bash

      # setup venv
      python3 -m venv .venv
      source .venv/bin/activate
      pip install -U pip setuptools wheel pre-commit nox

      # pre-commit configuration
      pre-commit install

#. Edit your code.
#. Commit your changes (ex. ``git commit -m 'Add some amazing-feature'``).
#. Push to the branch (ex. ``git push origin amazing-feature``).
#. Open a pull request

For the full details, see below.

Overview of How To Contribute to This Repository
================================================

To contribute to this repository, you first need to set up your own local repository:

* `Fork, clone, and branch the repo`_
* `Set up your local preview environment`_

After this initial setup, you then need to:

* `Sync local master branch with upstream master`_
* Edit the documentation in reStructured Text
* `Preview HTML changes locally`_
* Open a pull request / merge request.

Once a merge request gets approved, it can be merged!

Prerequisites
=============

For local development, the following prerequisites are needed:

* `git `__
* `Python 3.6+ `__
* `Ability to create python venv `__

Windows 10 Users
----------------

For the best experience, when contributing from a Windows OS to projects using
Python-based tools like ``pre-commit``, we recommend setting up `Windows
Subsystem for Linux (WSL) `__,
with the latest version being WSLv2.

The following gists on GitHub have been consulted with success for several
contributors:

* `Official Microsoft docs on installing WSL
  `__

* A list of PowerShell commands in a gist to `Enable WSL and Install Ubuntu
  20.04
  `__

  * Ensure you also read the comment thread below the main content for
    additional guidance about using Python on the WSL instance.

We recommend `Installing Chocolatey on Windows 10 via PowerShell w/ Some
Starter Packages
`__.
This installs ``git``, ``microsoft-windows-terminal``, and other helpful tools
via the awesome Windows package management tool, `Chocolatey
`__.

``choco install git`` easily installs ``git`` for a good Windows-dev
experience.  From the ``git`` package page on Chocolatey, the following are
installed:

* Git BASH
* Git GUI
* Shell Integration

Fork, Clone, and Branch the Repo
================================

This project uses the fork and branch Git workflow. For an overview of this
method, see `Using the Fork-and-Branch Git Workflow
`__.

* First, create a new fork into your personal user space.
* Then, clone the forked repo to your local machine.

  .. code-block:: bash

     # SSH or HTTPS
     git clone /idem-vmc.git

.. note::

    Before cloning your forked repo when using SSH, you need to create an SSH
    key so that your local Git repository can authenticate to the GitLab
    remote server.  See `GitLab and SSH keys
    `__ for instructions, or
    `Connecting to GitHub with SSH
    `__.

Configure the remotes for your main upstream repository:

.. code-block:: bash

    # Move into cloned repo
    cd idem-vmc

    # Choose SSH or HTTPS upstream endpoint
    git remote add upstream git-or-https-repo-you-forked-from

Create new branch for changes to submit:

.. code-block:: bash

    git checkout -b amazing-feature

Setup Your Local Preview Environment
=====================================

If you are not on a Linux machine, you need to set up a virtual environment to
preview your local changes and ensure the `prerequisites`_ are met for a Python
virtual environment.

From within your local copy of the forked repo:

.. code-block:: bash

    # Setup venv
    python3 -m venv .venv
    # If Python 3.6+ is in path as 'python', use the following instead:
    # python -m venv .venv

    # Activate venv
    source .venv/bin/activate
    # On Windows, use instead:
    # .venv/Scripts/activate

    # Install required python packages to venv
    pip install -U pip setuptools wheel pre-commit nox
    pip install -r requirements/base.txt

    # Setup pre-commit
    pre-commit install

``Pre-commit`` and ``Nox`` Setup
--------------------------------

This project uses `pre-commit `__ and
`nox `__ to make it easier for
contributors to get quick feedback, for quality control, and to increase
the chance that your merge request will get reviewed and merged.

``nox`` handles Sphinx requirements and plugins for you, always ensuring your
local packages are the needed versions when building docs. You can think of it
as ``Make`` with superpowers.

What is Pre-commit?
-------------------

``pre-commit`` is a tool that will automatically run
local tests when you attempt to make a git commit. To view what tests are run,
you can view the ``.pre-commit-config.yaml`` file at the root of the
repository.

One big benefit of pre-commit is that *auto-corrective measures* can be done
to files that have been updated. This includes Python formatting best
practices, proper file line-endings (which can be a problem with repository
contributors using differing operating systems), and more.

If an error is found that cannot be automatically fixed, error output will help
point you to where an issue may exist.

Sync Local Master Branch With Upstream Master
=============================================

If needing to sync feature branch with changes from upstream master, do the
following:

.. note::

    This will need to be done in case merge conflicts need to be resolved
    locally before a merge to master in the upstream repo.

.. code-block:: bash

    git checkout master
    git fetch upstream
    git pull upstream master
    git push origin master
    git checkout my-new-feature
    git merge master

Preview HTML Changes Locally
============================

To ensure that the changes you are implementing are formatted correctly, you
should preview a local build of your changes first. To preview the changes:

.. code-block:: bash

    # Activate venv
    source .venv/bin/activate
    # On Windows, use instead:
    # .venv/Scripts/activate

    # Generate HTML documentation with spinx
    cd docs
    make html

    # Sphinx website documentation is dumped to docs/build/html/*
    # You can view this locally
    # firefox example
    firefox docs/build/html/index.html

.. note::

    If you encounter an error, Sphinx may be pointing out formatting errors
    that need to be resolved in order to properly generate the docs.

Testing the Project
=========================

.. code-block:: bash

    # Output version of Python activated/available
    # python --version OR
    python3 --version

    # Run appropriate test
    # Ex. if Python 3.8.x
    cd tests
    pytest

This project is a ``pop`` project which makes use of ``pytest-pop``, a
``pytest`` plugin. For more information on ``pytest-pop``, and writing tests
for ``pop`` projects:

* `pytest-pop README `__
* `pytest documentation `__
