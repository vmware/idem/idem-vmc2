==========
!ARCHIVED!
==========

This project has been archived, along with all other POP and Idem-based projects.

* For more details: `Salt Project Blog - POP and Idem Projects Will Soon be Archived <https://saltproject.io/blog/2025-01-24-idem-pop-projects-archived/>`__

========
IDEM-VMC
========

.. image:: https://img.shields.io/badge/made%20with-pop-teal
   :alt: Made with pop, a Python implementation of Plugin Oriented Programming
   :target: https://pop.readthedocs.io/

.. image:: https://img.shields.io/badge/made%20with-python-yellow
   :alt: Made with Python
   :target: https://www.python.org/

This project enables `Idem <https://gitlab.com/saltstack/pop/idem>`__ users
to leverage `VMware vSphere Automation SDK for Python <https://github.com/vmware/vsphere-automation-sdk-python>`__
functionality to declare and enforce the state of cloud infrastructure,
applications, configurations, and more.

About
=====

VMware Cloud (a.k.a. VMC) redefine the foundation of IT to power every application on any 
cloud. With Multi-Cloud solutions from VMware, you can migrate to the cloud without 
recoding your apps, modernize your infrastructure, and operate consistently across the 
data center, the edge, and any cloud. This `Plugin Oriented Programming <https://gitlab.com/saltstack/pop/pop>`__ (POP)
implementation provides a plugin extension for Idem that enables declarative
state control of VMC resources.

This project is an early implementation of a modern POP plugin for VMC.
It does not yet cover all of VMC, in fact much work remains to do so. The
state of the project is a sufficient model to show how further
`POP Subs <https://pop.readthedocs.io/en/latest/topics/subs_overview.html>`__
can be reasonably written and added to the project.

What is POP?
------------

This project is built with `pop <https://pop.readthedocs.io/>`__, a Python-based
implementation of *POP*. POP seeks to bring together concepts and wisdom from the
history of computing in new ways to solve modern computing problems.

For more information:

* `Intro to Plugin Oriented Programming (POP) <https://pop-book.readthedocs.io/en/latest/>`__
* `pop-awesome <https://gitlab.com/saltstack/pop/pop-awesome>`__
* `pop-create <https://gitlab.com/saltstack/pop/pop-create/>`__



Getting Started
===============

In order to use this plugin, a few prerequisites must be satisfied and the plugin must be
installed in an environment supporting the prerequisites..

Prerequisites
-------------

* Python 3.6+; and
* git *(only if installing from source, or contributing to the project)*.

Installation
------------

Clone the `idem-vmc` repository and install with pip.

.. code:: bash

    git clone git@<your-project-path>/idem-vmc.git
    pip install -e idem-vmc

Usage
=====

After installation the VMC Idem Provider execution and state modules will be accessible 
to the pop `hub`. In order to use them we need to set up our credentials.
Follow the `instructions <https://docs.vmware.com/en/VMware-Cloud-services/services/Using-VMware-Cloud-Services/GUID-E2A3B1C1-E9AD-4B00-A6B6-88D31FCDDF7C.html>`_ to generate an api token.

Credentials with Acct
---------------------

Create a new file called `credentials.yaml` and populate it with credentials.
The `default` profile will be used automatically by `idem` unless you specify one 
with `--acct-profile=profile_name` on the cli.

There are many ways providers/profiles can be stored. See 
`acct backends <https://gitlab.com/saltstack/pop/acct-backends>`_
for more information.

A profile needs to specify the api key it uses and optionally the
UUID for your default org. The `default_org_id` will be used on any
state calls that do not specify an org UUID.

credentials.yaml

.. code:: yaml

    csp.token:
      default:
        refresh_token: dmd23q3au8ljyajcvhz207of4ivsn9vjiaxzez223qeagdpe0voqiasknykv58jt
        default_org_id: my_org


Now encrypt the credentials file and add the encryption key and encrypted file path to 
the ENVIRONMENT.

The `acct` command should be available as it is a requisite of `idem`.
Encrypt the the credential file.

.. code:: bash

    acct encrypt credentials.yaml

output::

    -A9ZkiCSOjWYG_lbGmmkVh4jKLFDyOFH4e4S1HNtNwI=

The command will generate a `credentials.yaml.fernet` file which has your encripted
credentials. If you wish, you can now delete the original `credentials.yaml` file.

Add these to your environment:

.. code:: bash

    export ACCT_KEY="-A9ZkiCSOjWYG_lbGmmkVh4jKLFDyOFH4e4S1HNtNwI="
    export ACCT_FILE=$PWD/credentials.yaml.fernet

Account Profiles
----------------

A profile can be specified for use with a specific state.
If no profile is specified, the profile called "default", if one exists, will be used:

.. code:: sls

    ensure_resource_exists:
        vmc.sddc.present:
            - acct_profile: my-staging-env
            - name: idem_vmc_dfw
            - kwarg1: val1

It can also be specified from the command line when executing states.

.. code:: bash

    idem state --acct-profile my-staging-env my_state.sls

It can also be specified from the command line when calling an 
exec module directly ( although calling execs directly is not recommended ).

.. code:: bash

    idem exec --acct-profile my-staging-env vmc.sddc.sddc.list

Example State (SLS) File
------------------------

You can create a state file that declares the desired state of various VMC
resources. For example, `sddc-create.sls <examples/sddc-create.sls>`__ creates a
SDDC. The file `sddc-delete.sls <examples/sddc-delete.sls>`__ deletes the same resources.

To create the sddc, you would issue a command similar to that below.

.. code-block:: bash

   (env) $ idem state examples/sddc-create.sls

**Note**: that Idem and POP implementations are both idempotent and asynchronous
in their operations. Therefore, you can run that same command multiple times
without harming the outcome intent of the state declarations.

To delete the sddc, you would issue a command simillar to that below.

.. code-block:: bash

   (env) $ idem state examples/sddc-delete.sls

As noted above, POP plugins are asynchronous by nature, therefore some of the
state declarations may fail because VMC will refuse to delete certain
resources, such a SDDC, when they are in certian states such as deploying.

In order to reconcile the errors, run the command multiple times until all
state delarations produce no errors.

A Word on Resource Management on VMC
-----------------------------------

As mentione above, you can use the idem-vmc plugin to manage resources on VMC. However,
if you decided to make changes to your resources ourside of this plugin, you must proceed
with caution. In the case of SDDCs for example, VMC allows you to create multiple 
SDDCs in the same `org` with the same name. At that point, it is much harder for the SDDC
state to manage the resource since the search for an SDDC happens by name. If the plugin finds
more than one SDDC with the same name, an error will be raised. At that point, there are two
choices, either make a manual change to delete or rename one of the SDDC or provide a filter.
In the filter you can specify a unique property about the SDDC you are looking for,
such as UUID.

Contributing
============

The idem-vmc project team welcomes contributions from the community. Before
you start working with idem-vmc, please read our
`Developer Certificate of Origin <https://cla.vmware.com/dco>`__. All
contributions to this repository must be signed as described on that page.
Your signature certifies that you wrote the patch or have the right to pass it
on as an open-source patch. For more detailed information, refer to
`CONTRIBUTING.rst. <CONTRIBUTING.rst>`__

Running Tests
-------------

Before running any tests, make sure to install the test dependencies by running the command:

.. code-block:: bash

    (env) $ pip install -r requirements-test.txt

When running integration tests, a configuration file must be created to hold the test
environment information. A `test_config.ini.sample <tests/integration/test_config.ini.sample>`__
is included to help get you started. First, copy the file to the same directory but 
name it `test_config.ini`. Next, fill in the required information. Without the information,
the integration tests will be skipped. Below is an example of what a filled out configuration
file looks like:

.. code-block:: ini

    [VMC]
    name_prefix = idem_vmc
    region = us-west-1
    provider = AWS
    customer_subnet_ids = subnet-12a3bcd4567e8910f
    connected_account_id = a123b456-7891-0123-4c56-789d0efgh12i

After the configuration file is in place, run all the tests like below.

.. code-block:: bash

    cd tests
    pytest

**Note**: Deploying an SDDC on VMC on AWS can take on average about two hours. All the 
integration tests run in parallel but you will still have to wait at least that much 
before they are complete.

Generating Documentation
------------------------

The `docs <docs>`__ has files that can be used to semi-auto generate documentation from docstrings in the code. 
Anytime you add new modules to the project, docstrings should be added. Once the new documented modules are in place,
add them to the `index.rst <docs/source/index.rst>`__ file and run:

.. code-block:: bash

    cd docs
    make html

The commands will generate HTML files in a folder located at `docs/build`. The files can be viewed in the web browser with
a command like this:

.. code-block:: bash

    open docs/build/html/index.html

Acknowledgements
================

* `Img Shields <https://shields.io>`__ for making repository badges easy.

Known Issues
============

idem-vmc uses the `Python vSphere Automation SDK <https://github.com/vmware/vsphere-automation-sdk-python>`__.
During the development of the project, a few issues have been raised againts
the the SDK. Here is the list of issues:

* https://github.com/vmware/vsphere-automation-sdk-python/issues/280

You are ready to use idem-vmc!!!
