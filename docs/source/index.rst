.. idem-vmc documentation master file, created by
   sphinx-quickstart on Fri Jul  2 10:10:15 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to idem-vmc's documentation!
====================================

States
======

.. automodule:: vmc.states.vmc.sddc
   :members:

.. automodule:: vmc.states.vmc.esx
   :members:

Contracts
=========

.. automodule:: vmc.states.vmc.recursive_contracts.init
   :members:

.. automodule:: vmc.exec.vmc.recursive_contracts.init
   :members:

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
