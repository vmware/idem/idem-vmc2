Ensure ESX in SDDC:
    vmc.esx.present:
        - org: "INSERT_ORG_UUID"
        - sddc:
            id:
                name: "demo-sddc"
        - num_hosts: 3