Create SDDC:
    vmc.sddc.present:
        - org: "INSERT_ORG_UUID"
        - name: "demo-sddc"
        - filters: []
        - region: "us-west-1"
        - provider: "AWS"
        - deployment_type: "SingleAZ"
        - num_hosts: 1
        - account_link_config:
            delay_account_link: False
        - account_link_sddc_config:
            - customer_subnet_ids:
                - "INSERT_SUBNET_ID"
                connected_account_id: 
                    id:
                        account_number:  "INSERT_AWS_ACCOUNT_NUMBER"

        