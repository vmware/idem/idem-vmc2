from tests.integration.test_helper import IntegrationTestHelper
import dict_tools
import mock
import random
import pytest
import asyncio
from typing import Any, Dict, List
import configparser
import os

# ================================================================================
# pop fixtures
# ================================================================================
@pytest.fixture(scope="session", autouse=True)
def acct_subs() -> List[str]:
    return ["vmc"]

# TODO add documentation around having this profile available in the
# the credentials.yaml. The test keep getting skipped but there is no
# indication as to why.
@pytest.fixture(scope="session", autouse=True)
def acct_profile() -> str:
    return "test_development_idem_vmc"


@pytest.fixture(scope="session")
def hub(hub):
    """
    Overriding pytest-pop's hub to add in this plugin's subs.
    
    :param hub: hub from pytest-pop.
    :return: yields the test hub plus the repo's subs.
    """

    hub.pop.sub.add(dyne_name="idem")
    with mock.patch("sys.argv", ["idem", "state"]):
        hub.pop.config.load(["idem", "acct"], "idem", parse_cli=True)
    yield hub

@pytest.mark.asyncio
@pytest.fixture(scope="module")
async def ctx(hub, acct_subs: List[str], acct_profile: str) -> Dict[str, Any]:
    """
    Overriding pytest-pop's ctx to add in this plugin's credentials.
    
    :param hub: hub from pytest-pop.
    :param acct_subs: List of acct subs to load.
    :param acct_profile: Name of the account profile with the 
        credentials to the test environment.
    :return: yields the test ctx.
    """

    ctx = dict_tools.data.NamespaceDict(run_name="test", test=False)
    if not hub.OPT.acct.acct_file:
        pytest.skip("Missing acct_file. Make sure you have the environment variable ACCT_FILE set")
    if not hub.OPT.acct.acct_key:
        pytest.skip("Missing acct_key. Make sure you have the environment variable ACCT_KEY set")
    
    # Add the profile to the account
    await hub.acct.init.unlock(hub.OPT.acct.acct_file, hub.OPT.acct.acct_key)
    if not hub.acct.UNLOCKED:
        pytest.skip(f"acct could not unlock {hub.OPT.acct.acct_file}")
   
    ctx.acct = await hub.acct.init.gather(acct_subs, acct_profile)

    # Test if the created ctx is functional; if not then skip all the integration tests that use it
    if not ctx.acct.refresh_token:
        pytest.skip("ctx is not configured correctly")

    # Initialize an instance of the test helper class
    hub.test_helper = IntegrationTestHelper(hub=hub, ctx=ctx)

    yield ctx

    # Close all connections when the tests are complete
    await hub.acct.init.close()

@pytest.fixture(scope='session')
def event_loop():
    '''Create an instance of the default event loop for each test case.'''
    
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()

#####################HELPER FUNCTIONS#################################

@pytest.fixture(scope="function")
def instance_name(config, config_section):
    ascii_lower = "abcdefghijklmnopqrstuvwxyz0123456789"
    rstring = "".join([random.choice(ascii_lower) for _ in range(20)])
    return f"{config.get(config_section, 'name_prefix')}_test_{rstring}"

@pytest.fixture(scope="session")
def config_filename():
    return 'test_config.ini'

@pytest.fixture(scope="session")
def config_section():
    return 'VMC'

@pytest.fixture(scope="session")
def customer_subnet_ids(config, config_section):
    return list(config.get(config_section, 'customer_subnet_ids').split(','))

@pytest.fixture(scope="session")
def config(config_filename, config_section):
    """
    Reads the specified file located in the current directory to
    configure the integration tests. If no file is found, the 
    integration tests will be skipped. Also, if any of the required 
    data is missing, the integration tests will be skipped.

    :param config_filename: The name of the configuration file to read.
        The file must be in the current directory for it to work.
    :param config_section: The name of the section in the
        configuration file to read from.
    """

    cf = configparser.ConfigParser()
    cf.read(os.path.join(os.path.dirname(__file__), config_filename))
    if not cf.has_section(config_section):
        pytest.skip("Missing config.ini file. Make sure you have configuration file in the current directory")
    if not cf.has_option(config_section, 'region'):
        pytest.skip("Missing 'region' in the config.ini file")
    if not cf.has_option(config_section, 'customer_subnet_ids'):
        pytest.skip("Missing 'customer_subnet_ids' in the config.ini file")
    if not cf.has_option(config_section, 'connected_account_id'):
        pytest.skip("Missing 'connected_account_id' in the config.ini file")
    if not cf.has_option(config_section, 'provider'):
        # Default to AWS when no provider is given
        cf.set(config_section, 'provider', 'AWS')
    if not cf.has_option(config_section, 'name_prefix'):
        # Default to empty string when no prefix given
        cf.set(config_section, 'name_prefix', '')

    return cf

@pytest.fixture(scope="function")    
def sddc_payload(ctx, instance_name, config, config_section, customer_subnet_ids):
    return {
        'org' : ctx.acct.default_org_id,
        'name': instance_name,
        'filters': [],
        'region': config.get(config_section, 'region'),
        'provider': config.get(config_section, 'provider'),
        'deployment_type': 'SingleAZ',
        'num_hosts': 1,
        'account_link_config': {
            'delay_account_link': False
        },
        'account_link_sddc_config': [
            {
                'customer_subnet_ids': customer_subnet_ids,
                'connected_account_id': config.get(config_section, 'connected_account_id')
            }
        ]
    }

@pytest.fixture(scope="session")
def timeout_min(config, config_section):
    if config.get(config_section, 'provider') == 'AWS':
        # An SDDC deployment typically takes about two hours
        # on VMC on AWS
        return 120
    return 20

@pytest.fixture(scope="session")
def poll_interval_sec(config, config_section):
    if config.get(config_section, 'provider') == 'AWS':
        # An SDDC deployment typically takes about two hours
        # on VMC on AWS which is why we only poll every 5 min
        return 300
    return 30