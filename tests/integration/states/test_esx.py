from vmc.helpers import constants
import pytest


@pytest.mark.asyncio
async def test_present_add_host(
    hub, 
    ctx, 
    instance_name, 
    sddc_payload,
    timeout_min,
    poll_interval_sec
    ):

     # Create SDDC with exec call
    org = sddc_payload.pop('org')
    old_num_host = sddc_payload.get('num_hosts')
    await hub.exec.vmc.sddc.init.create(
        ctx,
        org,
        sddc_config=sddc_payload
    )
    
    new_num_hosts = 3
    esx_state_kwargs = {
        'org' : org,
        'sddc': {
            'id': {
                'name': instance_name
            }
        },
        'num_hosts': new_num_hosts,

    }
    ret = await hub.states.vmc.esx.present(ctx, **esx_state_kwargs)
    assert ret[constants.RESULT] == False
    assert f'Sddc is currently not in a state to accept any additions/modifications. Please try once the status is READY' in ret[constants.COMMENT]
   
    # Wait for the SDDC to be ready to add ESXs
    ret = await hub.test_helper.wait_for_state(
        timeout_min, 
        hub.states.vmc.esx.present, 
        f'Operation to add {new_num_hosts} ESX hosts for SDDC "{instance_name}" STARTED.',
        esx_state_kwargs,
        poll_interval_sec
    )
    assert ret[constants.RESULT] == True
   
    # Wait for the ESXs to be added
    ret = await hub.test_helper.wait_for_state(
        timeout_min, 
        hub.states.vmc.esx.present, 
        f'{new_num_hosts} ESX hosts already present in SDDC "{instance_name}".',
        esx_state_kwargs,
        poll_interval_sec
    )
    assert ret[constants.RESULT] == True
    assert len(ret[constants.CHANGES]['old']['resource_config']['esx_hosts']) == new_num_hosts
    assert len(ret[constants.CHANGES]['new']['resource_config']['esx_hosts']) == new_num_hosts
     
    # Clean up
    await hub.exec.vmc.sddc.init.delete(
        ctx,
        org,
        ret[constants.CHANGES]['new']['id']
    )

@pytest.mark.asyncio
async def test_present_remove_host(
    hub, 
    ctx, 
    instance_name, 
    sddc_payload,
    timeout_min,
    poll_interval_sec
    ):

    # Create SDDC with exec call
    old_num_hosts = 3
    sddc_payload['num_hosts'] = old_num_hosts
    org = sddc_payload.pop('org')
    await hub.exec.vmc.sddc.init.create(
        ctx,
        org,
        sddc_config=sddc_payload
    )
    
    new_num_hosts = 1
    esx_state_kwargs = {
        'org' : org,
        'sddc': {
            'id': {
                'name': instance_name
            }
        },
        'num_hosts': new_num_hosts,

    }
    ret = await hub.states.vmc.esx.present(ctx, **esx_state_kwargs)
    assert ret[constants.RESULT] == False
    assert f'Sddc is currently not in a state to accept any additions/modifications. Please try once the status is READY' in ret[constants.COMMENT]
   
    # Wait for the SDDC to be ready to remove ESXs
    ret = await hub.test_helper.wait_for_state(
        timeout_min, 
        hub.states.vmc.esx.present, 
        f'Operation to remove {new_num_hosts} ESX hosts for SDDC "{instance_name}" STARTED.',
        esx_state_kwargs,
        poll_interval_sec
    )
    assert ret[constants.RESULT] == True
    assert len(ret[constants.CHANGES]['old']['resource_config']['esx_hosts']) == old_num_hosts
    assert len(ret[constants.CHANGES]['new']['resource_config']['esx_hosts']) == old_num_hosts
   
    # Wait for the ESXs to be removed
    ret = await hub.test_helper.wait_for_state(
        timeout_min, 
        hub.states.vmc.esx.present, 
        f'{new_num_hosts} ESX hosts already present in SDDC "{instance_name}".',
        esx_state_kwargs,
        poll_interval_sec
    )
    assert ret[constants.RESULT] == True
    assert len(ret[constants.CHANGES]['old']['resource_config']['esx_hosts']) == new_num_hosts
    assert len(ret[constants.CHANGES]['new']['resource_config']['esx_hosts']) == new_num_hosts
     
    # Clean up
    await hub.exec.vmc.sddc.init.delete(
        ctx,
        org,
        ret[constants.CHANGES]['new']['id']
    )
