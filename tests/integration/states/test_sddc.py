from vmc.helpers import constants
import pytest
import uuid



@pytest.mark.asyncio
async def test_present_create(
    hub, 
    ctx, 
    instance_name, 
    sddc_payload,
    timeout_min,
    poll_interval_sec
    ):

    org = sddc_payload.get('org')
    ret = await hub.states.vmc.sddc.present(ctx, **sddc_payload)

    assert ret[constants.RESULT] == True
    assert f'Creation of SDDC "{instance_name}" STARTED' in ret[constants.COMMENT]
    assert ret[constants.CHANGES]['old'] == {}
    assert ret[constants.CHANGES]['new']['name'] == f'{instance_name}'

    # Wait for the SDDC to be ready
    ret = await hub.test_helper.wait_for_state(
        timeout_min, 
        hub.states.vmc.sddc.present, 
        f'SDDC "{instance_name}" is already present with status "READY"', 
        sddc_payload,
        poll_interval_sec
    )
   
    assert ret[constants.CHANGES]['old']['sddc_state'] == 'READY'
    assert ret[constants.CHANGES]['old']['name'] == f'{instance_name}'
    assert ret[constants.CHANGES]['new'] == ret[constants.CHANGES]['old']

    # Check the SDDC is there
    sddc_res = await hub.exec.vmc.sddc.init.get(
        ctx, 
        org, 
        ret[constants.CHANGES]['new']['id']
    ) 
    assert sddc_res.get(constants.RESULT)
    assert sddc_res.get(constants.RET).name == f'{instance_name}'

    # Clean up
    await hub.exec.vmc.sddc.init.delete(
        ctx,
        org,
        ret[constants.CHANGES]['new']['id']
    )

@pytest.mark.asyncio
async def test_present_update(
    hub, 
    ctx, 
    instance_name, 
    sddc_payload,
    timeout_min,
    poll_interval_sec
    ):

    # Create SDDC
    org = sddc_payload.get('org')
    ret = await hub.states.vmc.sddc.present(ctx, **sddc_payload)

    # Wait for the SDDC to be ready
    ret = await hub.test_helper.wait_for_state(
        timeout_min, 
        hub.states.vmc.sddc.present, 
        f'SDDC "{instance_name}" is already present with status "READY"', 
        sddc_payload,
        poll_interval_sec
    )

    new_name = "test_update_" + str(uuid.uuid4())
    sddc_payload['new_name'] = new_name

    # Wait for the SDDC to update
    ret = await hub.test_helper.wait_for_state(
        timeout_min, 
        hub.states.vmc.sddc.present,
        f'SDDC "{instance_name}" was updated with the name "{new_name}".',  
        sddc_payload,
        poll_interval_sec
    )

    assert ret[constants.CHANGES]['old']['name'] == f'{instance_name}'
    assert ret[constants.CHANGES]['new']['name'] == f'{new_name}'
    assert ret[constants.CHANGES]['new']['id'] == ret[constants.CHANGES]['old']['id']

    # Clean up
    await hub.exec.vmc.sddc.init.delete(
        ctx,
        org,
        ret[constants.CHANGES]['new']['id']
    )

@pytest.mark.asyncio
async def test_absent(
    hub, 
    ctx, 
    instance_name, 
    sddc_payload,
    timeout_min,
    poll_interval_sec
    ):

    # Create SDDC with exec call
    org = sddc_payload.pop('org')
    await hub.exec.vmc.sddc.init.create(
        ctx,
        org,
        sddc_config=sddc_payload
    )

    # Delete SDDC with the absent state
    sddc_payload = {
        'org' : org,
        'name': instance_name,
        'filters': []
    }
    ret = await hub.states.vmc.sddc.absent(ctx, **sddc_payload)

    # First run will fail do to VMC REST API behavior.
    # You cannot delete an SDDC that is being created.
    assert ret[constants.RESULT] == False
    assert f'Sddc is currently not in a state where it can be deleted. Please try once the status is READY or FAILED.' in ret[constants.COMMENT]
    assert ret[constants.CHANGES] == {}

    # TODO adjust timeout since it takes a real SDDC about 2 hours to get deployed
    ret = await hub.test_helper.wait_for_state(
        timeout_min, 
        hub.states.vmc.sddc.absent, 
        f'Deletion of SDDC "{instance_name}" STARTED.', 
        sddc_payload,
        poll_interval_sec
    )

    assert ret[constants.RESULT] == True
    assert ret[constants.CHANGES]['old']['name'] == instance_name
    assert ret[constants.CHANGES]['new'] == {}

    ret = await hub.test_helper.wait_for_state(
        timeout_min, 
        hub.states.vmc.sddc.absent, 
        f'SDDC "{instance_name}" is already absent.', 
        sddc_payload,
        poll_interval_sec
    )

    assert ret[constants.CHANGES] == {}

    # Check the SDDC is not there anymore
    sddc_list_res = await hub.exec.vmc.sddc.init.list(
        ctx, 
        org, 
        filters=[
            {
                "name" : instance_name
            }
        ]
    )
    assert sddc_list_res.get(constants.RESULT)
    assert len(sddc_list_res.get(constants.RET)) == 0

@pytest.mark.asyncio
async def test_present_lookup(
    hub, 
    ctx, 
    instance_name, 
    sddc_payload,
    timeout_min,
    poll_interval_sec,
    config,
    config_section,
    customer_subnet_ids
    ):

    org = sddc_payload.get('org')
    
    # Query for the connected account
    connected_account_res = await hub.exec.vmc.account_link.connected_account.list(
        ctx, 
        org,
        filters=[{
            "id": config.get(config_section, 'connected_account_id')
        }]
    )               
    connected_account = connected_account_res[constants.RET][0]

    # Query for the compatible subnets
    subnet_res = await hub.exec.vmc.account_link.compatible_subnet.get(
        ctx,
        org, 
        linked_account_id=connected_account.id,
        region=config.get(config_section, 'region')
    )
    subnets = subnet_res[constants.RET]
    vpc_map = subnets.vpc_map
    filtered_subnets = []
    for v in vpc_map.values():
        for subnet in v.subnets:
            # Get the subnets with the given ids
            for subnet_id in customer_subnet_ids:
                if subnet.subnet_id == subnet_id:
                    filtered_subnets.append({
                        'id': {
                            'region': subnet.region_name,
                            'name': subnet.name
                        }
                    })

    # Build account_link_sddc_config                 
    sddc_payload['account_link_sddc_config'] = [
        {
            'customer_subnet_ids': filtered_subnets,
            'connected_account_id': {
                'id': {
                    'account_number':  connected_account.account_number
                }
            }
        }
    ]
    
    ret = await hub.states.vmc.sddc.present(ctx, **sddc_payload)

    assert ret[constants.RESULT] == True
    assert f'Creation of SDDC "{instance_name}" STARTED' in ret[constants.COMMENT]
    assert ret[constants.CHANGES]['old'] == {}
    assert ret[constants.CHANGES]['new']['name'] == f'{instance_name}'

    # Wait for the SDDC to be ready
    ret = await hub.test_helper.wait_for_state(
        timeout_min, 
        hub.states.vmc.sddc.present, 
        f'SDDC "{instance_name}" is already present with status "READY"', 
        sddc_payload,
        poll_interval_sec
    )
   
    assert ret[constants.CHANGES]['old']['sddc_state'] == 'READY'
    assert ret[constants.CHANGES]['old']['name'] == f'{instance_name}'
    assert ret[constants.CHANGES]['new'] == ret[constants.CHANGES]['old']

    # Check the SDDC is there
    sddc_res = await hub.exec.vmc.sddc.init.get(
        ctx, 
        org, 
        ret[constants.CHANGES]['new']['id']
    ) 
    assert sddc_res.get(constants.RESULT)
    assert sddc_res.get(constants.RET).name == f'{instance_name}'

    # Clean up
    await hub.exec.vmc.sddc.init.delete(
        ctx,
        org,
        ret[constants.CHANGES]['new']['id']
    )
