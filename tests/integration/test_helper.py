import time
from com.vmware.vmc.model_client import Task, Sddc
from vmc.helpers import constants

class IntegrationTestHelper:
    """
    Convenience class used for integration tests.

    Methods
    -------
    wait_for_task(org: str, task: Task, timeout: int) -> Task
        Polls a VMC Task every 10 secods until it completes or the `timeout` is reached.
    
    wait_for_sddc(org:str, name: str, timeout: int) -> Sddc
        Polls a VMC SDDC every 10 secods until it completes or the `timeout` is reached.

    wait_for_state(timeout: int, func, final_comment: str, func_params, poll_interval: int = 30)
        Polls a state until it reaches the desired comment or the `timeout` is reached. When the timeout
        is reached, the last :class:`vmc.helpers.models.StateReturn` is returned to the caller.
    """

    def __init__(self, hub, ctx):
        self._hub = hub
        self._ctx = ctx

    def wait_for_task(self, org: str, task: Task, timeout: int) -> Task:
        """
        Polls a VMC Task every 10 secods until it completes or the `timeout` is reached.

        :param org: Organization identifier.
        :param task: VMC task to poll.
        :param timeout: Time in minutes to wait.
        :rtype: :class:`com.vmware.vmc.model_client.Task`
        :return: com.vmware.vmc.model.Task
        :raise: :class:`TimeoutError`
            The task did not complete in the given time.
        """

        exit_time = time.time() + timeout * 60
        while True:
            task = self._hub.tool.vmc.API.get_instance(self._ctx).orgs.Tasks.get(
                org=org,
                task=task.id
            )
            if task.status == Task.STATUS_FINISHED:
                self._hub.log.info(f"Task {task.id} finished successfully")
                break
            elif task.status == Task.STATUS_FAILED:
                self._hub.log.error(f"Task {task.id} failed")
                raise ChildProcessError(f"Task failed {task.id}")
            elif task.status == Task.STATUS_CANCELED:
                self._hub.log.error(f"Task {task.id} cancelled")
                raise ChildProcessError(f"Task cancelled {task.id}")
            else:
                self._hub.log.debug(
                    f"Estimated time remaining: {task.estimated_remaining_minutes} minutes"
                )
                time.sleep(10)
            
            if time.time() > exit_time:
                raise TimeoutError(f'The task {task.id} was not complete after {timeout} minutes')
                
        return task

    def wait_for_sddc(self, org:str, name: str, timeout: int) -> Sddc:
        """
        Polls a VMC SDDC every 10 secods until it completes or the `timeout` is reached.

        :param org: Organization identifier.
        :param name: Name of the SDDC.
        :param timeout: Time in minutes to wait.
        :rtype: :class:`com.vmware.vmc.model_client.Sddc`
        :return: com.vmware.vmc.model.Sddc
        :raise: :class:`TimeoutError`
            The SDDC did not complete in the given time.
        """

        sddcs = self._hub.tool.vmc.API.get_instance(self._ctx).orgs.Sddcs.list(org)
        sddc_found = None
        for sddc in sddcs:
            if sddc.name == name:
                sddc_found = sddc
                break
        if not sddc_found:
            raise Exception(f'SDDC "{name}" not found')
            
        exit_time = time.time() + timeout * 60
        while True:
            sddc = self._hub.tool.vmc.API.get_instance(self._ctx).orgs.Sddcs.get(
                org,
                sddc_found.id
            )
            if sddc.sddc_state == Sddc.SDDC_STATE_READY:
                self._hub.log.info(f"SDDC {sddc.id} is ready")
                break
            elif sddc.sddc_state == Sddc.SDDC_STATE_FAILED:
                self._hub.log.error(f"SDDC {sddc.id} failed")
                raise ChildProcessError(f"SDDC failed {sddc.id}")
            elif sddc.sddc_state == Sddc.SDDC_STATE_CANCELED:
                self._hub.log.error(f"SDDC {sddc.id} cancelled")
                raise ChildProcessError(f"SDDC cancelled {sddc.id}")
            else:
                time.sleep(10)
            
            if time.time() > exit_time:
                raise TimeoutError(f'SDDC {sddc.id} was not ready after {timeout} minutes')
                
        return sddc

    async def wait_for_state(self,
        timeout: int, 
        func, 
        final_comment: str, 
        func_params, 
        poll_interval: int = 30):
        """
        Polls a state until it reaches the desired comment or the `timeout` is reached. When the timeout
        is reached, the last :class:`vmc.helpers.models.StateReturn` is returned to the caller.


        :param timeout: Time in minutes to wait.
        :param func: The state function to call.
        :param final_comment: The final comment expected from the state.
        :param func_params: Keyword arguments to pass directly to the state function.
        :type poll_interval: :class:`int`, optional
        :param poll_interval: The interval of time in seconds to wait between calls to the state
            function. Defaults to 30 seconds.
        """

        exit_time = time.time() + timeout * 60
        while True:
            ret = await func(self._ctx, **func_params)
            if ret[constants.RESULT]:
                if ret[constants.COMMENT] == final_comment:
                    self._hub.log.info(f'State "{func}" finished successfully')
                    return ret
            
            self._hub.log.debug(f'State "{func}" in progress')
            time.sleep(poll_interval)
            if time.time() > exit_time:
                self._hub.log.error(f'State "{func}" was not complete after {timeout} minutes')
                return ret