# -*- coding: utf-8 -*-
"""
VMC subsystem pytest unit test configurations.

Copyright (c) 2021 VMware, Inc. All Rights Reserved.
SPDX-License-Identifier: Apache-2.0
"""

import pytest
import dict_tools
import asyncio
import mock
from typing import Any, Dict, List


@pytest.fixture(scope="session")
def hub(hub):
    """
    Overriding pytest-pop's hub to add in this plugin's subs.
    
    :param hub: hub from pytest-pop
    :return: yields the test hub plus the repo's subs
    """

    hub.pop.sub.add(dyne_name="idem")
    with mock.patch("sys.argv", ["idem", "state"]):
        hub.pop.config.load(["idem", "acct"], "idem", parse_cli=True)
    yield hub

@pytest.mark.asyncio
@pytest.fixture(scope="function")
async def ctx(hub, acct_subs: List[str], acct_profile: str) -> Dict[str, Any]:
    ctx = dict_tools.data.NamespaceDict(run_name="test", test=False)
    yield ctx

@pytest.fixture(scope='session')
def event_loop():
    '''Create an instance of the default event loop for each test case.'''
    
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()
