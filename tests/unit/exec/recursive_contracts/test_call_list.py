# -*- coding: utf-8 -*-
"""
VMC subsystem pytest call_list contract test.

Copyright (c) 2021 VMware, Inc. All Rights Reserved.
SPDX-License-Identifier: Apache-2.0
"""

import pytest
import vmc.exec.vmc.recursive_contracts.init as contracts

class TestStub():
    def __init__(self, name, num_host):
        self.name = name
        self.num_host = num_host

# Arrange
def stub(name, num_host):
    return TestStub(name=name, num_host=num_host)

# Arrange
@pytest.fixture
def stub_list(list_length: int = None):
    stubs = []
    if not list_length:
        list_length = 3
    for count in range(list_length):
        stubs.append(stub(f'Stub-{count}', count + 1))

    return stubs

# Arrange
@pytest.fixture
def stub_list_closure(stub_list):
    async def return_stub_list(*args, **kwargs):
        return stub_list
    return return_stub_list

@pytest.mark.asyncio
async def test_single_filter_with_matching_value(hub, ctx, stub_list_closure, stub_list):
    ctx.func = stub_list_closure
    ctx.args = []
    ctx.kwargs = { 
        "filters" : [
            {
                "name" : "Stub-0"
            }
        ]
    }
    ret = await contracts.call_list(hub, ctx)
    assert len(ret) == 1
    assert ret == stub_list[:1]

@pytest.mark.asyncio
async def test_single_filter_with_no_matching_value(hub, ctx, stub_list_closure, stub_list):
    ctx.func = stub_list_closure
    ctx.args = []
    ctx.kwargs = { 
        "filters" : [
            {
                "name" : "NameNotInList"
            }
        ]
    }
    ret = await contracts.call_list(hub, ctx)
    assert len(ret) == 0

@pytest.mark.asyncio
async def test_multiple_filters_with_different_keys_and_matching_values(hub, ctx, stub_list_closure, stub_list):
    ctx.func = stub_list_closure
    ctx.args = []
    ctx.kwargs = { 
        "filters" : [
            {
                "name" : stub_list[0].name
            },
            {
                "num_host" : stub_list[0].num_host
            }
        ]
    }
    ret = await contracts.call_list(hub, ctx)
    assert len(ret) == 1
    assert ret == stub_list[:1]

@pytest.mark.asyncio
async def test_multiple_filters_with_same_key_and_different_values(hub, ctx, stub_list_closure, stub_list):
    ctx.func = stub_list_closure
    ctx.args = []
    ctx.kwargs = { 
        "filters" : [
            {
                "name" : stub_list[0].name
            },
            {
                "name" : "AnotherStubName"
            }
        ]
    }
    ret = await contracts.call_list(hub, ctx)
    assert len(ret) == 0

@pytest.mark.asyncio
async def test_multiple_filters_with_same_key_and_values(hub, ctx, stub_list_closure, stub_list):
    ctx.func = stub_list_closure
    ctx.args = []
    ctx.kwargs = { 
        "filters" : [
            {
                "name" : stub_list[0].name
            },
            {
                "name" : stub_list[0].name
            }
        ]
    }
    ret = await contracts.call_list(hub, ctx)
    assert len(ret) == 1
    assert ret == stub_list[:1]

@pytest.mark.asyncio
async def test_filter_key_not_in_object(hub, ctx, stub_list_closure, stub_list):
    ctx.func = stub_list_closure
    ctx.args = []
    ctx.kwargs = { 
        "filters" : [
            {
                "not_a_field_in_object" : 1
            }
        ]
    }
    ret = await contracts.call_list(hub, ctx)
    assert len(ret) == 0

@pytest.mark.asyncio
async def test_single_filter_with_multiple_values_matching(hub, ctx, stub_list_closure, stub_list):
    ctx.func = stub_list_closure
    ctx.args = []
    ctx.kwargs = { 
        "filters" : [
            {
                "name" : [stub_list[0].name, stub_list[1].name]
            }
        ]
    }
    ret = await contracts.call_list(hub, ctx)
    assert len(ret) == 2
    assert ret == stub_list[0:2]

@pytest.mark.asyncio
async def test_single_filter_with_multiple_values_some_matching(hub, ctx, stub_list_closure, stub_list):
    ctx.func = stub_list_closure
    ctx.args = []
    ctx.kwargs = { 
        "filters" : [
            {
                "name" : [stub_list[0].name, "NotInTheList", None]
            }
        ]
    }
    ret = await contracts.call_list(hub, ctx)
    assert len(ret) == 1
    assert ret == stub_list[:1]

@pytest.mark.asyncio
async def test_single_filter_with_multiple_values_none_matching(hub, ctx, stub_list_closure, stub_list):
    ctx.func = stub_list_closure
    ctx.args = []
    ctx.kwargs = { 
        "filters" : [
            {
                "name" : ["HelloWorld", "NotInTheList"]
            }
        ]
    }
    ret = await contracts.call_list(hub, ctx)
    assert len(ret) == 0