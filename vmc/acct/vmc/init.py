# -*- coding: utf-8 -*-
"""
Acct implementation module.

Copyright (c) 2021 VMware, Inc. All Rights Reserved.
SPDX-License-Identifier: Apache-2.0

This file implements the `gather` function to get the
credential information for VMC from the `credentials.yaml` file.

Documentation for acct: 
https://pypi.org/project/acct/
"""

async def gather(hub):
    """
    Authentication to the VMC API is handled internally by the vSphere automation SDK.
    The function gathers the credentials from the `credentials.yaml` file and makes
    them available in the context.

    :param hub: The redistributed pop central hub.
    """
    sub_profiles = {}
    for profile, ctx in hub.acct.PROFILES.get("csp.token", {}).items():
        sub_profiles[profile] = ctx

    return sub_profiles