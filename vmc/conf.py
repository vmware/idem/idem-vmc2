# https://pop.readthedocs.io/en/latest/tutorial/quickstart.html#adding-configuration-data

# In this dictionary goes all the immutable values you want to show up 
# under hub.OPT.vmc
CONFIG = {
    "config": {
        "default": None,
        "help": "Load extra options from a configuration file onto hub.OPT.vmc",
    }
}

# The selected subcommand for your cli tool will show up under 
# hub.SUBPARSER. The value for a subcommand is a dictionary that will 
# be passed as kwargs to argparse.ArgumentParser.add_subparsers
SUBCOMMANDS = {
    # "my_sub_command": {}
}

# Include keys from the CONFIG dictionary that you want to expose on 
# the cli. The values for these keys are a dictionaries that will be 
# passed as kwargs to argparse.ArgumentParser.add_option
CLI_CONFIG = {
    # Connection options
    "acct_key": {"source": "acct", "subcommands": ["_global_"]},
    "acct_file": {"source": "acct", "subcommands": ["_global_"]},
    "acct_profile": {"source": "idem", "subcommands": ["_global_"]},
    # Config reading options
    "config": {"options": ["-c"], "subcommands": ["_global_"]}
}

# These are the namespaces that your project extends. The hub will 
# extend these keys with the modules listed in the values
DYNE = {
    "exec": ["exec"],
    "acct": ["acct"],
    "states": ["states"],
    "tool": ["tool"],
    "vmc": ["vmc"],
}
