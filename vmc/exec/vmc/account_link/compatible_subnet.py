# -*- coding: utf-8 -*-
"""
Execs for CompatibleSubnets in VMC.

Copyright (c) 2021 VMware, Inc. All Rights Reserved.
SPDX-License-Identifier: Apache-2.0

Documentation for the VMC compatible subnets API:
https://developer.vmware.com/docs/vmc/latest/account-link/compatible-subnets/
"""

from vmc.helpers import exceptions

def _closure(hub, target: str):
    """
    Finds a function in the VMC SDK CompatibleSubnets client and generates an
    async function wrapper that accepts `args` and `kwargs`.

    :param hub: The redistributed pop central hub.
    :param target: The name of the function
    """

    async def _compatible_subnets_closure(ctx, *args, **kwargs):
        """
        Function wrapper for a VMC SDK CompatibleSubnets client function.

        :type ctx: :class:`dict`
        :param ctx: Information of the execution of the Idem run 
            located in `hub.idem.RUNS[ctx['run_name']]`.
        :param args: List of arguments to pass directly to the
            VMC SDK function.
        :param kwargs: Dict of keyword arguments to pass directly to the
            VMC SDK function.
        :raise: :class:`vmc.helpers.exceptions.ClientFunctionNotFoundError` 
             Cannot find the function with the given name.
        """

        vmc = hub.tool.vmc.API.get_instance(ctx)
        # Find the class in the VMC SDK for the resource then
        # call the function specified as a parameter with the
        # *args and **kwargs.
        client = vmc.orgs.account_link.CompatibleSubnets
        try:
            return getattr(client, target)(*args, **kwargs)
        except AttributeError as e:
            raise exceptions.ClientFunctionNotFoundError(client, target) from e

    return _compatible_subnets_closure

def __func_alias__(hub):
    """
    Adds the list of available VMC SDK functions for CompatibleSubnets to the POP hub.

    Documentation for `__func_alias__`:
    https://pop.readthedocs.io/en/stable/topics/func_alias.html

    :param hub: The redistributed pop central hub.
    """

    closures = {}
    for func_name in (
        "get",
    ):
        closures[func_name] = _closure(hub, func_name)
    
    return closures
