# -*- coding: utf-8 -*-
"""
Exec `Contracts`_ module.

Copyright (c) 2021 VMware, Inc. All Rights Reserved.
SPDX-License-Identifier: Apache-2.0

This file implements contracts related to all VMC execs.

.. _Contracts: https://pop.readthedocs.io/en/latest/topics/contracts.html#contracts
"""

from typing import Dict, Any
from http import HTTPStatus
from vmc.helpers.models import ExecReturn
from vmc.helpers import exceptions


async def call_list(hub, ctx):
    """
    Call contract for all `list` execs. The contract implements filtering
    on top of VMC APIs that do not have filtering capabilities such as SDDCs.
    The filters are passed in the `ctx.kwargs` as the keyword 'filters'. 
    Filters format: [{object.attr : value}]
    
    The contract ensures that any exceptions generated are wrapped in an 
    Exception that inherits from :class:`vmc.helpers.exceptions.BaseException`.

    :param hub: The redistributed pop central hub.
    :type ctx: :class:`dict`
    :param ctx: Information of the execution of the Idem run 
        located in `hub.idem.RUNS[ctx['run_name']]`.
    """

    filters = None
    if 'filters' in ctx.kwargs:
       filters = ctx.kwargs.pop('filters')

    try:
        objects = await ctx.func(*ctx.args, **ctx.kwargs)
        if filters:
            list_filter = _list_filter_closure(filters)
            objects = filter(list_filter, objects)
            return list(objects)

        return objects
    except Exception as e:
        if hub.SUBPARSER == "exec":
            # If exec was called from the command line then quit here
            # States and tests might want to keep the connection open after a failure
            await hub.acct.init.close()
        raise exceptions.wrap_exception(e)

async def call(hub, ctx):
    """
    Call contract for any exec that is not a `list`. The contract ensures that any exceptions generated 
    are wrapped in an Exception that inherits from :class:`vmc.helpers.exceptions.BaseException`.

    :param hub: The redistributed pop central hub.
    :type ctx: :class:`dict`
    :param ctx: Information of the execution of the Idem run 
        located in `hub.idem.RUNS[ctx['run_name']]`.
    """

    try:
        return await ctx.func(*ctx.args, **ctx.kwargs)
    except Exception as e:
        if hub.SUBPARSER == "exec":
            # If exec was called from the command line then quit here
            # States and tests might want to keep the connection open after a failure
            await hub.acct.init.close()
        raise exceptions.wrap_exception(e)


async def post(hub, ctx) -> Dict[str, Any]:
    """
    Post contract for all execs. The contract ensures the results from an exec
    get wrapped in a :class:`vmc.helpers.models.ExecReturn`.

    :param hub: The redistributed pop central hub.
    :type ctx: :class:`dict`
    :param ctx: Information of the execution of the Idem run 
        located in `hub.idem.RUNS[ctx['run_name']]`.
    """

    # Build a response object with the data we got back from the
    # exec call and return it
    return ExecReturn(
        comment=HTTPStatus.OK.value,
        result=True,
        ret=ctx.ret
    )

###############HELPER FUNCTIONS####################

def _list_filter_closure(filters):
    """
    The function returns a filter function for a list with the given filters.
    
    :type :class:`list`
    :param filters: Filters to use to form the filter function. Format:
    [{object.attr : value}] 
    """
    
    def _list_filter(obj):
        """
        Function wrapper to be used with the standard Python `filter` function.
        In essence, the filter can do 'and' and 'or' for a given object.
        The format for 'and' is a list of dicts. Example, when passing a
        filter like this [{'color': 'red'} , {'count': 3}], the list returns
        all objects which have the attribute 'color' with the value 'red' and
        the attribute 'count' with the value of 3.
        The format for 'or' on a given object attribute is by providing a list
        of values per attribute. Example, when passing a filter like this
        [{'color': ['red', 'blue']}], the list returns all
        objects which have the attribute 'color' with the values 'red' or 'blue'.

        :param obj: The object to apply the filters to.
        """

        match = True
        or_match = False
        for f in filters:
            for key in f:
                if hasattr(obj, key):
                    if isinstance(f[key], list):
                        for or_value in f[key]:
                            if getattr(obj, key) == or_value:
                                or_match = True
                                break
                        if not or_match:
                            match = False
                            break
                    else:
                        if getattr(obj, key) != f[key]:
                            match = False
                            break
                else:
                    match = False
                    break

            if not match:
                break

        return match

    return _list_filter