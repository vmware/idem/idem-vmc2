# -*- coding: utf-8 -*-
"""
VMC sub exceptions module.

Copyright (c) 2021 VMware, Inc. All Rights Reserved.
SPDX-License-Identifier: Apache-2.0
"""

from com.vmware.vapi.std import errors_client

class BaseError(Exception):
    """Base exception class for all exceptions to inherit."""

    def __init__(
        self, 
        error: Exception = None, 
        message: str = None
    ) -> None:
        """
        Initialize an instance of this class. You can pass either
        an Exception or a string. The exception will take precedence
        over the string.

        :type error: :class:`Exception`, optional
        :param error: Exception to present when rendering the 
            exception.
        :type message: :class:`str`, optional
        :param message: String message to present when rendering 
            the exception.
        """

        if error:
            self.error = error
        elif message:
            self.error = message
        super().__init__(self.error)

class APIClientError(BaseError):
    """
    Exception class for all VMC Python SDK exceptions to inherit.
    """
    def __str__(self) -> str:
        """
        Override to convert the VMC Python SDK exception to a string.
        """
        
        if hasattr(self.error, 'to_json'):
            e_str = self.error.to_json()
        elif hasattr(self.error, 'to_dict'):
            e_str = str(self.error.to_dict())
        elif hasattr(self.error, '__dict__'):
            e_str = str(self.error.__dict__)
        else:
            e_str = str(self.error)

        return e_str
    

class NotFoundError(APIClientError):
    """Exception raised for resources not found in VMC."""
    pass

class UnauthenticatedError(APIClientError):
    """
    Exception raised for when a method requires authentication and 
    the user is not authenticated in VMC.
    """
    pass

class UnauthorizedError(APIClientError):
    """
    Exception raised for when the user is not authorized to perform 
    the method in VMC.
    """
    pass

class InvalidRequestError(APIClientError):
    """
    Exception raised for when the request is malformed in a way that 
    VMC is unable to process it.
    """
    pass

class InternalServerError(APIClientError):
    """
    Exception raised for when VMC encounters an unexpected condition that 
    prevented it from fulfilling the request. 
    """
    pass

class InvalidArgumentError(APIClientError):
    """
    Exception raised for when the values received by VMC for one or more 
    parameters are not acceptable.
    """
    pass

class ClientFunctionNotFoundError(BaseError):
    """
    Exception raised for when the user requests a function from a VMC
    API set that does not exist.
    """

    def __init__(self, client, func_name: str) -> None:
        """
        Initialize an instance of this class.

        :param client: The VMC SDK client object where the function was
            not found.
        :param func_name: Name of the function not found.
        """
        
        super().__init__(message=f'Function "{func_name}" not found in the API client {client._VAPI_SERVICE_ID}')

#################HELPER METHODS###########################

def wrap_exception(err: Exception):
    """
    Convenience method to translate the VMC Python SDK errors to errors
    defined in this module.

    :param err: The exception to translate.
    """    
    if type(err) is errors_client.NotFound:
        return NotFoundError(err)
    elif type(err) is errors_client.Unauthenticated:
        return UnauthenticatedError(err)
    elif type(err) is errors_client.Unauthorized:
        return UnauthorizedError(err)
    elif type(err) is errors_client.InvalidRequest:
        return InvalidRequestError(err)
    elif type(err) is errors_client.InternalServerError:
        return InternalServerError(err)
    elif type(err) is errors_client.InvalidArgument:
        return InvalidArgumentError(err)
    elif type(err) is errors_client.Error:
        # Wrapper for all unspecified errors that come from VMC SDK
        return APIClientError(err)  
    else:
        # Return the error as is when no match found
        return err