# -*- coding: utf-8 -*-
"""
VMC sub convenience classes module.

Copyright (c) 2021 VMware, Inc. All Rights Reserved.
SPDX-License-Identifier: Apache-2.0

This file has classes common across all exec and states. 
"""

from typing import Any
from vmc.helpers import constants

class StateReturn(dict):
    """
    Convenience class used to manage POP state returns. The 
    underlaying dict has four keys: 'name', 'result', 'comment' 
    and 'changes'.

    Methods
    -------
    set_result(result: bool)
        Sets the 'result' key in the dict.
    """

    def __init__(self, 
            result: bool = None, 
            old_obj: Any = None, 
            new_obj: Any = None, 
            comment: str = None):
        """
        Initialize an object of this class. The objects pass to the
        `old_obj` and `new_obj` parameters will get converted and 
        stored as dictionary versions of themselves.

        :param result: True if the state call works, False otherwise.
        :param old_obj: For state changes, the object's state
            prior to any state change request executions.
        :param new_obj: For state changes, the new object's state 
            after any state change request executions.
        :param comment: Any relevant comments to the state execution
            such as status code, exception, or other context for the 
            given result.
        """

        super(StateReturn, self).__init__({
            constants.NAME: None,
            constants.RESULT: result,
            constants.COMMENT: comment,
            constants.CHANGES: {}
        })
        
        try:
            try:
                old = old_obj.to_dict()
            except AttributeError:
                old = vars(old_obj)
        except TypeError:
            old = old_obj

        try:
            try:
                new = new_obj.to_dict()
            except AttributeError:
                new = vars(new_obj)
        except TypeError:
            new = new_obj

        if old or new:
            self[constants.CHANGES] = {
                'old': old,
                'new': new
            }

    def set_result(self, result: bool):
        """
        Sets the 'result' key in the dict.

        :param result: Value to store in the 'result' key.
        """

        super().__setitem__(constants.RESULT, result)


class ExecReturn(dict):
    """
    Convenience class used to manage POP exec returns. The 
    underlaying dict has three keys: 'result', 'ret' and 'comment'.
    """

    def __init__(self,
            result: bool = None, 
            ret: Any = None,
            comment: str = None):
        """
        Initialize an object of this class.

        :param result: True if the exec call works, False otherwise.
        :param ret: The return data from the exec module run.
        :param comment: Any relevant comments to the exec execution
            such as status code, exception, or other context for the 
            given result.
        """

        super(ExecReturn, self).__init__({
            constants.RESULT: result,
            constants.RET: ret,
            constants.COMMENT: comment
        })