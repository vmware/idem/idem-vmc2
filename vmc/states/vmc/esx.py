"""
VMC `ESX`_ state module.

Copyright (c) 2021 VMware, Inc. All Rights Reserved.
SPDX-License-Identifier: Apache-2.0

This file implements states related to com.vmware.vmc.orgs.sddcs.esxs.
VMC credentials must be presented via the `acct Sub`.

.. _ESX: https://developer.vmware.com/docs/vmc/latest/sddcs/esxs/
"""

from vmc.helpers.models import StateReturn
from vmc.helpers import constants
import copy

async def present(
    hub, 
    ctx,  
    sddc,
    num_hosts: int,
    org: str = None, 
    **kwargs
    ):
    """
    Ensure the given number of `ESX`_ hosts exists within an SDDC.

    :param hub: The redistributed pop central hub.
    :type ctx: :class:`dict`
    :param ctx: Information of the execution of the Idem run 
        located in `hub.idem.RUNS[ctx['run_name']]`.
    :param sddc: The target SDDC. Two types of values are available. The first 
        is the SDDC UUID. The second is a dict with key-values that
        describe the SDDC. When passing a dict, make sure the attributes
        are unique enough to match only 1 SDDC. The dict will look like:
        {'id': {object.attr : value}}
    :param num_hosts: The number of ESX hosts expected to exists 
        in the SDDC.
    :type org: :class:`str`, optional 
    :param org: Organization identifier in the form of a UUID.
        If no identifier is provided, the one in the `credentials.yaml`
        file is used when specified.
    :param kwargs: Keyword args passed directly to the VMC API.

    Example usage:

    .. code-block:: yaml

        Ensure ESX in SDDC:
            vmc.esx.present:
                - org: {{ org_uuid }}
                - sddc:
                    id:
                        - name: "demo-sddc"
                - num_hosts: 3
    """

    kwargs_copy = copy.deepcopy(kwargs)
    # Get the Org. Default to the default_or_id set in the credentials.yml
    org = org if org else ctx.acct.default_org_id

    # Search for an SDDC within the org. We look for both
    # name and update name in case it was updated in a previous
    # state run
    if isinstance(sddc, dict):
        f = sddc['id']
        sddc_list_res = await hub.exec.vmc.sddc.init.list(
            ctx, 
            org, 
            filters=[f]
        ) 
        
        sddc_list = sddc_list_res[constants.RET]
        if not len(sddc_list) == 1:
            raise Exception(f'{len(sddc_list)} SDDCs found with the filters "{f}". Update your filters to return at most 1 and try again.')
        sddc = sddc_list[0]
    elif isinstance(sddc, str):
         # Query for new SDDC details
        sddc_res = await hub.exec.vmc.sddc.init.get(
            ctx, 
            org, 
            sddc
        ) 

        sddc = sddc_res[constants.RET]
    else:
        raise Exception(f'The "sddc" parameter must be a UUID or a dict describing an SDDC of the form {{id:{{property1="value"}}}}')
    
    # Check for any Tasks already in progress to add or remove esx hosts
    task_query = f"(resource_id eq {sddc.id}) and (task_type eq 'ESX-PROVISION'" \
        + " or task_type eq 'ESX-PROVISION-RESOURCES'" \
        + " or task_type eq 'ESX-DELETE-SINGLE-HOST'" \
        + " or task_type eq 'ESX-DELETE-VALIDATION'" \
        + " or task_type eq 'ESX-DELETE')"
    task_res = await hub.exec.vmc.task.list(
        ctx,
        org,
        task_query
    )
    tasks = task_res[constants.RET]
    for t in tasks:
        if t.status == 'STARTED' or t.status == 'CANCELING':
            return StateReturn(
                comment=f'The number of ESX for SDDC "{sddc.name}" is currently being updated.',
                old_obj=sddc,
                new_obj=sddc
            )

    # Check the current number of ESX and prepare payload
    if len(sddc.resource_config.esx_hosts) == num_hosts:
        return StateReturn(
            comment=f'{num_hosts} ESX hosts already present in SDDC "{sddc.name}".',
            old_obj=sddc,
            new_obj=sddc
        )
    elif len(sddc.resource_config.esx_hosts) < num_hosts:
        action = 'add'
        kwargs_copy['num_hosts'] = num_hosts - len(sddc.resource_config.esx_hosts)
    else:
        action = 'remove'
        kwargs_copy['num_hosts'] = len(sddc.resource_config.esx_hosts) - num_hosts
        

    esx_res = await hub.exec.vmc.sddc.esx.create(
        ctx,
        org,
        sddc.id,
        esx_config=kwargs_copy,
        action=action
    )

    task = esx_res[constants.RET]
    # Query for new SDDC details
    sddc_res = await hub.exec.vmc.sddc.init.get(
        ctx, 
        org, 
        sddc.id
    )

    return StateReturn(
        comment=f'Operation to {action} {num_hosts} ESX hosts for SDDC "{sddc.name}" {task.status}.', 
        old_obj=sddc, 
        new_obj=sddc_res[constants.RET]
    )
