# -*- coding: utf-8 -*-
"""
State `Contracts`_ module.

Copyright (c) 2021 VMware, Inc. All Rights Reserved.
SPDX-License-Identifier: Apache-2.0

This file implements contracts related to all VMC states.

.. _Contracts: https://pop.readthedocs.io/en/latest/topics/contracts.html#contracts
"""

from vmc.helpers.models import StateReturn

async def call(hub, ctx):
    """
    Call contract for all states. The contract ensures that any
    exceptions generated from any states are wrapped in a 
    :class:`vmc.helpers.models.StateReturn`.

    :param hub: The redistributed pop central hub.
    :type ctx: :class:`dict`
    :param ctx: Information of the execution of the Idem run 
        located in `hub.idem.RUNS[ctx['run_name']]`.
    """

    try: 
        state_return = await ctx.func(*ctx.args, **ctx.kwargs)
        state_return.set_result(True)
        return state_return
    except Exception as e:
        return StateReturn(
            comment= str(e),
            result=False,
        ) 
