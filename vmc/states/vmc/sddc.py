# -*- coding: utf-8 -*-
"""
VMC `SDDC`_ state module.

Copyright (c) 2021 VMware, Inc. All Rights Reserved.
SPDX-License-Identifier: Apache-2.0

This file implements states related to com.vmware.vmc.orgs.sddcs.
VMC credentials must be presented via the `acct Sub`.

.. _SDDC: https://developer.vmware.com/docs/vmc/latest/sddcs/sddcs/
"""

from vmc.helpers import constants
import copy
from typing import Any, Dict, List
from vmc.helpers.models import StateReturn

async def present(
    hub, 
    ctx,  
    name: str,
    org: str = None,
    filters: List[Dict[str, Any]] = [],
    new_name: str = None,
    **kwargs
    ):
    """
    Ensure a SDDC exists in the Org according to the requested state.

    :param hub: The redistributed pop central hub.
    :type ctx: :class:`dict`
    :param ctx: Information of the execution of the Idem run 
        located in `hub.idem.RUNS[ctx['run_name']]`.
    :param name: The name of the SDDC.
    :type org: :class:`str`, optional 
    :param org: Organization identifier in the form of a UUID.
        If no identifier is provided, the one in the `credentials.yaml`
        file is used when specified.
    :param filters: A list of attributes to help search for an existing
        SDDC. The list should be specific enough to narrow down the
        results to at most 1 SDDC. Filters will look like:
        [{object.attr : value}]
    :type new_name: :class:`str`, optional  
    :param new_name: Name to be used to update the name of an 
        existing SDDC.
    :param kwargs: Keyword args passed directly to the VMC API.

    Example usage:

    .. code-block:: yaml

        Ensure SDDC Present:
            vmc.sddc.present:
                - org: {{ org_uuid }}
                - name: "demo-sddc"
                - filters: []
                - region: {{ region }}
                - provider: "AWS"
                - deployment_type: "SingleAZ"
                - num_hosts: 1
                - account_link_config:
                    delay_account_link: False
                - account_link_sddc_config:
                    - customer_subnet_ids:
                        - {{ subnet_uuid }}
                    connected_account_id: {{ connected_account_uuid }}
    """

    kwargs_copy = copy.deepcopy(kwargs)
    filters_copy = copy.deepcopy(filters)
    sddc_config = {}
    names = []
    names.append(name)
    if new_name:
        names.append(new_name)

    # Get the Org. Default to the default_or_id set in the credentials.yml
    org = org if org else ctx.acct.default_org_id

    # Search for an SDDC within the org. We look for both
    # name and update name in case it was updated in a previous
    # state run
    filters_copy.append({'name' : names})
    sddc_list_res = await hub.exec.vmc.sddc.init.list(
        ctx, 
        org, 
        filters=filters_copy
    ) 
    
    sddc_list = sddc_list_res[constants.RET]
    if len(sddc_list) > 1:
        raise Exception(f'{len(sddc_list)} SDDCs found with the filters "{filters_copy}". Update your filters to return at most 1 and try again.')
    elif len(sddc_list) == 0:
        # CREATE - SDDC not found triggers creation

        # Extract optional parameters for create
        validate_only = None
        if 'validate_only' in kwargs_copy:
            validate_only = kwargs_copy.pop('validate_only')

        # Build payload to create SDDC
        sddc_config = kwargs_copy
        sddc_config['name'] = name
        try:
            for account_link_config in sddc_config['account_link_sddc_config']:
                if isinstance(account_link_config['connected_account_id'], str):
                    pass # The connected account ID is already set
                else:
                     # Lookup the connected account ID
                    f = account_link_config['connected_account_id']['id']
                    connected_account_res = await hub.exec.vmc.account_link.connected_account.list(
                                    ctx, 
                                    org,
                                    filters=[f]
                                )
                    
                    connected_accounts = connected_account_res[constants.RET]
                    if len(connected_accounts) != 1:
                        raise Exception(f'{len(connected_accounts)} objects found with filters "{f}". Filters should only match one object.')

                    account_link_config['connected_account_id'] = connected_accounts[0].id
                
                subnet_ids = []
                for customer_subnet_id in account_link_config['customer_subnet_ids']:
                    if isinstance(customer_subnet_id, str):
                        subnet_ids.append(customer_subnet_id)
                    else:
                        # Lookup the compatible subnet IDs
                        f = customer_subnet_id['id']
                        subnet_name = f['name']
                        subnet_region = f['region']
                        subnet_res = await hub.exec.vmc.account_link.compatible_subnet.get(
                                    ctx,
                                    org, 
                                    linked_account_id=account_link_config['connected_account_id'], 
                                    region=subnet_region
                                )

                        subnets = subnet_res[constants.RET]
                        vpc_map = subnets.vpc_map
                        for v in vpc_map.values():
                            for subnet in v.subnets:
                                # Get the subnet with the given name
                                if subnet.name == subnet_name:
                                    if not subnet.compatible:
                                        raise Exception(f'The subnet "{subnet_name}" is not compatible to deploy an SDDC')
                                    subnet_ids.append(subnet.subnet_id)    
                        if len(subnet_ids) == 0:
                            raise Exception(f'No compatible subnets where found with the filters "{f}"')       
                account_link_config['customer_subnet_ids'] = subnet_ids
        except KeyError:
            pass

        # Create the SDDC with the given configuration
        sddc_res = await hub.exec.vmc.sddc.init.create(
            ctx,
            org,
            sddc_config=sddc_config,
            validate_only=validate_only
        )

        # Query for new SDDC details
        task = sddc_res[constants.RET] 
        sddc_res = await hub.exec.vmc.sddc.init.get(
            ctx, 
            org, 
            task.resource_id
        ) 

        sddc = sddc_res[constants.RET]    
        return StateReturn(
            comment=f'Creation of SDDC "{name}" {task.status}.',
            old_obj={},
            new_obj=sddc
        ) 
        

    # READ - Return SDDC which is already present
    sddc = sddc_list[0]
    if sddc.name == name:
        if not new_name:
            # The SDDC is already present with the name and no new_name was given
            return StateReturn(
                comment=f'SDDC "{name}" is already present with status "{sddc.sddc_state}"',
                old_obj=sddc,
                new_obj=sddc
            )
    elif sddc.name == new_name:
        return StateReturn(
            comment=f'SDDC "{name}" was updated with the name "{new_name}"', 
            old_obj=sddc, 
            new_obj=sddc
        )

    # UPDATE - Only the name of the SDDC can change.
    
    # Build the exec payload
    sddc_config['name'] = new_name

    # Update the SDDC
    sddc_res = await hub.exec.vmc.sddc.init.patch(
        ctx,
        org,
        sddc.id,
        sddc_patch_request=sddc_config
    )

    return StateReturn(
        comment=f'SDDC "{name}" was updated with the name "{new_name}".', 
        old_obj=sddc, 
        new_obj=sddc_res[constants.RET]
    )

async def absent(
    hub, 
    ctx,  
    name: str, 
    org: str = None,
    filters: List[Dict[str, Any]] = [],
    **kwargs
    ):
    """
    Ensure a SDDC does not exist in the Org.

    :param hub: The redistributed pop central hub.
    :type ctx: :class:`dict`
    :param ctx: Information of the execution of the Idem run 
        located in `hub.idem.RUNS[ctx['run_name']]`.
    :param name: The name of the SDDC.
    :type org: :class:`str`, optional 
    :param org: Organization identifier in the form of a UUID.
        If no identifier is provided, the one in the `credentials.yaml`
        file is used when specified.
    :param filters: A list of attributes to help search for an existing
        SDDC. The list should be specific enough to narrow down the
        results to at most 1 SDDC. Filters will look like:
        [{object.attr : value}]
    :param kwargs: Keyword args passed directly to the VMC API.

    Example usage:

    .. code-block:: yaml
    
        Ensure SDDC Absent:
            vmc.sddc.absent:
                - org: {{ org_uuid }}
                - name: "demo-sddc"
                - filters: []
    """

    kwargs_copy = copy.deepcopy(kwargs)
    filters_copy = copy.deepcopy(filters)

    # Get the Org. Default to the default_or_id set in the credentials.yml
    org = org if org else ctx.acct.default_org_id
    
    # Search for an SDDC within the org with the filters
    filters_copy.append({'name' : name})
    sddc_list_res = await hub.exec.vmc.sddc.init.list(
        ctx, 
        org, 
        filters=filters_copy
    )
    
    sddc_list = sddc_list_res[constants.RET]
    if len(sddc_list) > 1:
        raise Exception(f'{len(sddc_list)} SDDCs found with the filters "{filters_copy}". Update your filters to return at most 1 and try again.')
    elif len(sddc_list) == 0:
        return StateReturn(
            comment=f'SDDC "{name}" is already absent.'
        )

    # DELETE
    sddc = sddc_list[0]
    sddc_res = await hub.exec.vmc.sddc.init.delete(
        ctx,
        org,
        sddc.id,
        **kwargs_copy
    )

    task = sddc_res[constants.RET]  
    return StateReturn(
        comment=f'Deletion of SDDC "{name}" {task.status}.', 
        old_obj=sddc, 
        new_obj={}
    )
