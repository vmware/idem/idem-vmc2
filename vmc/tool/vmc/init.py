# -*- coding: utf-8 -*-
"""
vSphere Automation Python SDK wrapper. The file will create an
instance of the :class:`VMCApi`, and assign it to the hub in
the location `hub.tool.vmc.API`.

Copyright (c) 2021 VMware, Inc. All Rights Reserved.
SPDX-License-Identifier: Apache-2.0
"""
# Import Python libs
import requests

# Import third party libs
try:
    from vmware.vapi.vmc.client import VmcClient, create_vmc_client
    HAS_LIBS = (True,)
except ImportError as e:
    HAS_LIBS = False, str(e)

def __virtual__(hub):
    return HAS_LIBS

def __init__(hub):
    hub.tool.vmc.API = VMCApi(hub=hub)

class VMCApi:
    """
    Python SDK wrapper used to make REST API calls to VMC.

    Methods
    -------
    get_instance(ctx)
        Factory method to get an instance of this class.
    authenticate(ctx)
        Acquire VMC Credentials. The API token from the contex
        will be used to authenticate the client against VMC.
    """
    
    def __init__(self, hub):
        self._hub = hub

    def get_instance(self, ctx):
        """
        Factory method to get an instance of this class.

        :type ctx: :class:`dict`
        :param ctx: Information of the execution of the Idem run 
            located in `hub.idem.RUNS[ctx['run_name']]`.
        """
        api = VMCApi(hub=self._hub)
        api.authenticate(ctx)
        return api
        
    def authenticate(self, ctx):
        """
        Acquire VMC Credentials. The API token from the contex
        will be used to authenticate the client against VMC.

        :type ctx: :class:`dict`
        :param ctx: Information of the execution of the Idem
            run located in `hub.idem.RUNS[ctx['run_name']]`.
        """
        session = requests.Session()
        if ctx.acct.get("vmc_url") and ctx.acct.get("csp_url"):
            self._vmc_client = VmcClient(
                session=session, 
                refresh_token=ctx.acct.refresh_token, 
                vmc_url=ctx.acct.get("vmc_url"), 
                csp_url=ctx.acct.get("csp_url")
            )
        else:
            self._vmc_client = create_vmc_client(
                refresh_token=ctx.acct.refresh_token,
                session=session
            )

    def __getattr__(self, attr: str):
        """
        Override to get existing resource from the VMC python SDK when
        using dot notation. For example, VMCApi.orgs will use this
        function to get the 'orgs' attribute from the python SDK.

        :param attr: The key of the resource to get.
        """
        return getattr(self._vmc_client, attr)
